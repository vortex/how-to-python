#coding=utf-8
__author__ = 'Administrator'
'''
'''
import socket
BUF_SIZE = 1024000
host = 'localhost'
port = 8083

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen(10) #并发数
while True:
    client, address = server.accept()
    data = client.recv(BUF_SIZE)
    client.send(data)
    print(data.decode()) #python3 要使用decode
    # client.close()