#coding=utf-8
'''
线程处理
'''
__author__ = 'Administrator'
import threading
import time
import os

path = os.getcwd()
file = path + '/log.txt'


num = 0
lock = threading.Lock()
class ThreadTest(threading.Thread):
    def run(self):
        global num
        time.sleep(1)
        lock.acquire()
        num = num + 1
        f = open(file, 'a')
        f.writelines('%s -- %d \n' % (self.name, num))
        print('%s -- %d' % (self.name, num))
        lock.release()

#开启多线程
jobs = []
for i in range(10):
    t = ThreadTest()
    jobs.append(t)

for th in jobs:
    th.start()

# for th in jobs:
#     th.join()

print('thread end...')


