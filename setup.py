import sys
from cx_Freeze import setup, Executable

base = None

if sys.platform == 'win32':
    base = 'Win32GUI'

packages = [

        ]

options = {
}

executables = [Executable('QtTest.py', base=base)]

setup(
    name="datatester",
    version="1.0.0",
    url='http://zengrong.net/',
    author='zrong',
    author_email='zrongzrong@gmail.com',
    description="A data tester for HHL.",
    options=options,
    executables=executables,
)