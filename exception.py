#coding=utf-8
'''
异常处理，抛出异常
'''
__author__ = 'Administrator'
try :
    open('log.txt')
except Exception as err:
    print(err)
finally :
    print('finally..')

print('-' * 10)

try :
    open('log.txt')
except :
    print('exception ..')

print('-' * 10)

try :
    open('log.txt')
except Exception:
    print('exception ..')

print('-' * 10)

#自定义抛出异常
try :
    raise Exception('input error', 'error other')
except Exception as e:
    print(e.args[0])