#coding=utf-8
'''
线程中使用Queue
'''
__author__ = 'Administrator'
import threading
import queue
import random
import time

que = queue.Queue()

class Producer(threading.Thread):
    def __init__(self, que):
        self._queue = que
        threading.Thread.__init__(self)

    def run(self):
        # time.sleep(3)
        while True:
            num = random.randint(0, 9999)
            self._queue.put(num)
            print('producer : ' + self.name + ' - ' + str(num))
            time.sleep(1)


class Consumer(threading.Thread):
    def __init__(self, que):
        self._queue = que
        threading.Thread.__init__(self)

    def run(self):
        # time.sleep(1)
        while True:
            num = self._queue.get()
            # self._queue.task_done()
            print('consumer : ' + self.name + ' - ' + str(num))
            time.sleep(1)


ths = []
for i in range(10):
    t = Producer(que)
    ths.append(t)

    t = Consumer(que)
    ths.append(t)

for th in ths:
    th.start()

# for th in ths:
#     th.join()

print('thread end ...')
print(sum(range(1000, -1000, -2)))

